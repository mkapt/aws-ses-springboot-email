package com.aws.email.service;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.MessageRejectedException;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.aws.email.builder.AWSConverter;
import com.aws.email.builder.MailTextBodyProcessor;
import com.aws.email.client.SESClient;
import com.aws.email.dto.EmailAWS;
import com.aws.email.model.Email;
import com.aws.email.repository.EmailRepository;
import com.aws.email.request.EmailRequest;
import com.aws.email.service.EmailService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
public class EmailServiceTest {
	
	@TestConfiguration
	static class EmailServiceTestConfiguration {
		@Bean
		public EmailService emailService() {
			return new EmailService();
		}
		
		@Bean
		@Primary
		public AWSConverter awsConverter() {
			return new AWSConverter();
		}
	}
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private AWSConverter awsConverter;
	
	@MockBean
	private EmailRepository emailRepository;
	
	@MockBean
	private SESClient sesClient;
	
	@MockBean 
	private MailTextBodyProcessor mailTextBodyProcessor;
	
	@MockBean
	private AmazonSimpleEmailService amazonSimpleEmailService;
	
	@Before
	public void before() {
		Email email = new Email("email@to.com","mensagem","assunto");
		when(emailRepository.save(email)).thenReturn(email);
		when(emailRepository.findById(anyLong())).thenReturn(email);
		when(sesClient.getStandardClient()).thenReturn(amazonSimpleEmailService);
	}
	
	@Test
	public void deveBuscarEmailComSucesso() {
		Email email = emailService.buscar(1L);
		assertNotNull(email);
	}
	
	@Test
	public void deveEnviarEmailComSucesso() {
		EmailRequest emailRequest = converterJsonParaRequest("json_valido.json", EmailRequest.class);
		when(mailTextBodyProcessor.processarTextBody(emailRequest)).thenReturn(anyString());
		EmailAWS emailAWS = awsConverter.toEmailAWS(emailRequest);
		SendEmailRequest request = new SendEmailRequest()
				.withSource(emailRequest.getEmailOrigem())
                .withDestination(emailAWS.getEmailTo())
                .withMessage(emailAWS.getMessage());
		when(amazonSimpleEmailService.sendEmail(request)).thenReturn(new SendEmailResult());
		SendEmailResult result = amazonSimpleEmailService.sendEmail(request);
		assertNotNull(result);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=MessageRejectedException.class)
	public void naoDeveEnviarEmailComSucesso() {
		EmailRequest emailRequest = converterJsonParaRequest("json_valido.json", EmailRequest.class);
		when(mailTextBodyProcessor.processarTextBody(emailRequest)).thenReturn(anyString());
		EmailAWS emailAWS = awsConverter.toEmailAWS(emailRequest);
		SendEmailRequest request = new SendEmailRequest()
				.withSource(emailRequest.getEmailOrigem())
                .withDestination(emailAWS.getEmailTo())
                .withMessage(emailAWS.getMessage());
		when(amazonSimpleEmailService.sendEmail(request)).thenThrow(MessageRejectedException.class);
		amazonSimpleEmailService.sendEmail(request);
	}
	
	private <T> T converterJsonParaRequest(String file, Class<T> clazz) {
		
		final String CAMINHO = "src/test/resources/json/";
		
		try {
			Path pathObject = Paths.get(CAMINHO.concat(file));
			return new ObjectMapper().readValue(Files.readAllBytes(pathObject), clazz);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
