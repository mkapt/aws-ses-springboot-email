--Arquivo criado manualmente para prepopular a base do H2
--Para ser lido, usar: spring.jpa.hibernate.ddl-auto=create-drop
--no application.properties
INSERT INTO t_email (id, email_to, message, subject) VALUES (1, 'destino1', 'mensagem1', 'assunto');
INSERT INTO t_email (id, email_to, message, subject) VALUES (2, 'destino2', 'mensagem2', 'assunto');
INSERT INTO t_email (id, email_to, message, subject) VALUES (3, 'destino3', 'mensagem3', 'assunto');