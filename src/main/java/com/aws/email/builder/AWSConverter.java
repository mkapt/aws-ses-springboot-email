package com.aws.email.builder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.aws.email.dto.EmailAWS;
import com.aws.email.request.EmailRequest;

/**
 * Converter de requisição em objeto da classe EmailAWS
 * @author michel.apt
 *
 */
@Component
public class AWSConverter {

	@Autowired
	private MailTextBodyProcessor processor;
	
	/**
	 * Converte requisição em objeto do tipo EmailAWS
	 * @param request
	 * @return EmailAWS
	 */
	public EmailAWS toEmailAWS(EmailRequest request) {
		EmailAWS emailAWS = new EmailAWS();
		emailAWS.setEmailTo(new Destination().withToAddresses(request.getEmailDestino()));
		emailAWS.setSubject(new Content().withData(request.getAssunto()));
		emailAWS.setTextBody(new Content().withData(processor.processarTextBody(request)));
		emailAWS.setBody(new Body().withHtml(emailAWS.getTextBody()));
		emailAWS.setMessage(new Message().withBody(emailAWS.getBody()).withSubject(emailAWS.getSubject()));
		return emailAWS;
	}
}
