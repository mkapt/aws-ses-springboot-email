package com.aws.email.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aws.email.dto.StatusEnvio;
import com.aws.email.model.Email;
import com.aws.email.request.EmailRequest;
import com.aws.email.response.EmailResponse;
import com.aws.email.service.EmailService;

@RestController
@RequestMapping(value="/email", produces=APPLICATION_JSON_VALUE)
public class EmailController {
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private EmailService emailService;
	
	@PostMapping(consumes=APPLICATION_JSON_VALUE)
	public ResponseEntity<EmailResponse> enviar(@RequestBody @Valid EmailRequest emailRequest) {
		
		try {
			Email email = emailService.enviar(emailRequest);
			return new ResponseEntity<EmailResponse>(new EmailResponse(email.getId(), emailRequest.getEmailDestino(), StatusEnvio.SUCESSO), HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("Erro ao enviar email", e);
			return new ResponseEntity<EmailResponse>(new EmailResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<Email> consultar(@PathVariable Long id) {
		Email emailEncontrado = emailService.buscar(id);
		if (emailEncontrado == null) {
			LOGGER.info("E-mail nao encontrado");
			return new ResponseEntity<Email>(new Email(), HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Email>(emailEncontrado, HttpStatus.OK);
	}
}
