package com.aws.email.repository;


import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.aws.email.model.Email;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EmailRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private EmailRepository emailRepository;
	
	@Test
	public void deveBuscarEmailComSucesso() {
		Email emailPersistido = new Email("emailTo@emaiTo.com", "mensagem", "assunto");
		entityManager.persist(emailPersistido);
		entityManager.flush();
		
		Email emailEncontrado = emailRepository.findById(emailPersistido.getId());
		
		assertEquals(emailEncontrado.getId(), emailPersistido.getId());
	}
}
