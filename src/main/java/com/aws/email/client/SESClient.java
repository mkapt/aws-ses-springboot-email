package com.aws.email.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;

/**
 * Classe client para o AWS Simple Email Service. A interface EnvironmentAware foi utilizada
 * para que se pudesse utilizar a classe Environment sem retornar null (a inje��o de depend.
 * acontecia tardiamente, depois que a vari�vel environment era acessada por meio do m�todo
 * getProperty() 
 * @author michel.apt
 *
 */
@Configuration
public class SESClient implements EnvironmentAware {

	private Environment environment;
	
	@Value("${credentials.path}")
	private String credentialsFilePath;
	
	@Value("${credentials.profile}")
	private String credentialsProfile;
	
	@Override
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	/**
	 * Usar quando as credenciais n�o est�o armazenadas na pasta padr�o .aws
	 * @return
	 */
	public AmazonSimpleEmailService getStandardClient() {
		return AmazonSimpleEmailServiceClientBuilder.standard()
    		.withCredentials(
				new ProfileCredentialsProvider(
					environment.getProperty("credentials.path"), environment.getProperty("credentials.profile")))
    		.build();
	}
	
	/**
	 * Usar quando as credenciais est�o na pasta padr�o .aws
	 * @return
	 */
	public AmazonSimpleEmailService getDefaultClient() {
		return AmazonSimpleEmailServiceClientBuilder.defaultClient();
	}
}
