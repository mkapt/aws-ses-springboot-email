package com.aws.email;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.aws.email.dto.StatusEnvio;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Model> processarErroDeValidacao(MethodArgumentNotValidException ex, Model model) {
		BindingResult result = ex.getBindingResult();
		
		for (FieldError error : result.getFieldErrors()) {
			String valor = extrairValorDoCampoComErro(result, error);
			String mensagem = null;
			if(!StringUtils.isEmpty(valor)) {
				mensagem = "[".concat(valor).concat("] ").concat(error.getDefaultMessage());
			}
			mensagem = "[".concat(error.getField()).concat("] ").concat(error.getDefaultMessage());
			
			model.addAttribute("codigo", StatusEnvio.FALHA.getCodigo());
            model.addAttribute("mensagem", StatusEnvio.FALHA.getMensagem().concat(mensagem));
		}
		
		return new ResponseEntity<>(model, HttpStatus.BAD_REQUEST);
		
	}
	
	private String extrairValorDoCampoComErro(BindingResult result, FieldError error) {
    	Object valor = result.getFieldValue(error.getField());
    	return valor != null ? valor.toString() : new String(""); 
    }
}
