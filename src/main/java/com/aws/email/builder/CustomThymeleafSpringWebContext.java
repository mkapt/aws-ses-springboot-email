package com.aws.email.builder;

import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;
import org.springframework.core.convert.ConversionService;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.thymeleaf.context.IWebContext;
import org.thymeleaf.context.VariablesMap;
import org.thymeleaf.spring4.context.SpringWebContext;

public class CustomThymeleafSpringWebContext implements IWebContext {

	private final SpringWebContext springWebContext;

    public CustomThymeleafSpringWebContext(
            final HttpServletRequest request, 
            final HttpServletResponse response,
            final ServletContext servletContext,
            final ApplicationContext applicationContext,
            final ConversionService conversionService,
            final Map<String, Object> dados) {
        // Create delegating SpringWebContext
        final Locale locale = RequestContextUtils.getLocale(request);
        springWebContext = new SpringWebContext(
            request, response, servletContext, locale, dados, applicationContext);
    }
    
	@Override
	public VariablesMap<String, Object> getVariables() {
		return springWebContext.getVariables();
	}

	@Override
	public Locale getLocale() {
		return springWebContext.getLocale();
	}

	@Override
	public void addContextExecutionInfo(String templateName) {
		springWebContext.addContextExecutionInfo(templateName);
	}

	@Override
	public HttpServletRequest getHttpServletRequest() {
		return springWebContext.getHttpServletRequest();
	}

	@Override
	public HttpServletResponse getHttpServletResponse() {
		return springWebContext.getHttpServletResponse();
	}

	@Override
	public HttpSession getHttpSession() {
		return springWebContext.getHttpSession();
	}

	@Override
	public ServletContext getServletContext() {
		return springWebContext.getServletContext();
	}

	@Override
	public VariablesMap<String, String[]> getRequestParameters() {return null;}

	@Override
	public VariablesMap<String, Object> getRequestAttributes() {return null;}

	@Override
	public VariablesMap<String, Object> getSessionAttributes() {return null;}

	@Override
	public VariablesMap<String, Object> getApplicationAttributes() {return null;}
}
