package com.aws.email.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.aws.email.model.Email;

@Repository
public interface EmailRepository extends CrudRepository<Email, Long> {
	
	public Email findById(Long id);
	
}
