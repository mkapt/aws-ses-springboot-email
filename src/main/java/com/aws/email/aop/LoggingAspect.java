package com.aws.email.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

	private Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Pointcut("target(com.aws.email.service.EmailService) && execution(com.aws.email.model.Email buscar(java.lang.Long))")
	public void pointcutMetodoBuscar(){}
	
	@After("execution(@com.aws.email.aop.Logging * *(..))")
	public void chamarMetodoAnotado(JoinPoint pjp) {
		LOGGER.info("Chamou aspectj com annotation");
	}
	
	@AfterThrowing(pointcut="execution(@com.aws.email.aop.Logging * *(..))", throwing = "e")
	public void logarException(JoinPoint pjp, Exception e) {
		LOGGER.info("Exception", e);
	}
	
	@After("pointcutMetodoBuscar()")
	public void chamarMetodoEspecifico(JoinPoint pjp) {
		LOGGER.info("Aspect de metodo �nico");
	}
}
