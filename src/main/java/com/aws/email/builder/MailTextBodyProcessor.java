package com.aws.email.builder;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.IWebContext;

import com.aws.email.request.EmailRequest;

@Component
public class MailTextBodyProcessor {
	
	@Autowired
	private TemplateEngine templateEngine;
	
	@Autowired
	private ServletContext servletContext; 
	
	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	private HttpServletRequest req;
	
	@Autowired
	private HttpServletResponse res;
	
	@Autowired
	private ConversionService conversionService;
	
	
	/**
	 * Gera um contexto simples a ser utilizado pelo Thymeleaf na cria��o do template.
	 * @param dados
	 * @return
	 */
	public Context getSimpleContext(Map<String, Object> dados) {
		Context context = new Context();
		context.setVariables(dados);
        return context;
	}
	
	/**
	 * Gera um contexto web utilizado pelo Thymeleaf para processamento do corpo do email.
	 * Algumas anota��es mais complexas de HTML necessitam de informa��es providas pelo
	 * contexto web.
	 * @param dados Dados a serem manipulados no template
	 * @return IWebContext do Thymeleaf
	 */
	public IWebContext getWebContext(Map<String, Object> dados) {
		return new CustomThymeleafSpringWebContext(req, res, servletContext, applicationContext, conversionService, dados);
	}
	
	/**
	 * Processa o corpo do email a ser enviado
	 * @param emailRequest Requisicao de email
	 * @return String com o corpo do email com o texto processado
	 */
	public String processarTextBody(EmailRequest emailRequest) {
		return templateEngine.process(emailRequest.getDocumento(), getWebContext(emailRequest.getDados()));
	}
}
