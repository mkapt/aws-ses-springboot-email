package com.aws.email.response;

import com.aws.email.dto.StatusEnvio;

public class EmailResponse {

	private String emailDestino;
	
	private StatusEnvio statusEnvio;
	
	private Long id;

	public EmailResponse(){}
	
	public EmailResponse(String emailDestino){
		this.emailDestino = emailDestino;
	}
	
	public EmailResponse(String emailDestino, StatusEnvio statusEnvio) {
		this.emailDestino = emailDestino;
		this.statusEnvio = statusEnvio;
	}
	
	public EmailResponse(Long id, String emailDestino, StatusEnvio statusEnvio) {
		this.id = id;
		this.emailDestino = emailDestino;
		this.statusEnvio = statusEnvio;
	}

	public String getEmailDestino() {
		return emailDestino;
	}

	public void setEmailDestino(String emailDestino) {
		this.emailDestino = emailDestino;
	}
	
	public StatusEnvio getStatusEnvio() {
		return statusEnvio;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
