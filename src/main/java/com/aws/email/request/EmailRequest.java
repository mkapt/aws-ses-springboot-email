package com.aws.email.request;

import java.util.Map;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.aws.email.dto.EmailAWS;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EmailRequest {

	@NotEmpty
	@Email
	private String emailOrigem;
	
	@NotEmpty
	@Email
	private String emailDestino;
	
	private String documento;
	
	private String assunto;
	
	private Map<String, Object> dados;
	
	public String getEmailOrigem() {
		return emailOrigem;
	}

	public void setEmailOrigem(String emailOrigem) {
		this.emailOrigem = emailOrigem;
	}

	public String getEmailDestino() {
		return emailDestino;
	}

	public void setEmailDestino(String emailDestino) {
		this.emailDestino = emailDestino;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	
	public Map<String, Object> getDados() {
		return dados;
	}

	public void setDados(Map<String, Object> dados) {
		this.dados = dados;
	}

	@Override
	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return "{}";
	}
	
	/**
	 * Converte o DTO EmailAWS para objeto de dominio Email
	 * @param emailAWS
	 * @return Email
	 */
	public com.aws.email.model.Email toEmail(EmailAWS emailAWS) {
		com.aws.email.model.Email email = new com.aws.email.model.Email();
		email.setEmailTo(this.emailDestino);
		email.setMessage(emailAWS.getTextBody().getData());
		email.setSubject(this.assunto);
		return email;
	}
}
