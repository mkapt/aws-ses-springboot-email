package com.aws.email.dto;

public enum StatusEnvio {

	SUCESSO("00", "E-mail enviado com sucesso. "),
	FALHA("01", "E-mail nao enviado. ");
	
	private String codigo;
	private String mensagem;
	
	StatusEnvio(String codigo, String mensagem) {
		this.codigo = codigo;
		this.mensagem = mensagem;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
}
