package com.aws.email.aop;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotation que indica ponto de log no sistema
 * @author michel.apt
 *
 */
@Retention(RUNTIME)
@Target({ TYPE, METHOD })
public @interface Logging {}
