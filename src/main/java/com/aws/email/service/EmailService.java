package com.aws.email.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.aws.email.aop.Logging;
import com.aws.email.builder.AWSConverter;
import com.aws.email.client.SESClient;
import com.aws.email.dto.EmailAWS;
import com.aws.email.model.Email;
import com.aws.email.repository.EmailRepository;
import com.aws.email.request.EmailRequest;

@Service
public class EmailService {
	
	private Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SESClient client;
	
	@Autowired
	private EmailRepository emails;
	
	@Autowired
	private AWSConverter converter;
	
	@Logging
	public Email enviar(EmailRequest emailRequest) throws Exception {
		EmailAWS emailAWS = converter.toEmailAWS(emailRequest);
		
		SendEmailRequest request = new SendEmailRequest()
				.withSource(emailRequest.getEmailOrigem())
                .withDestination(emailAWS.getEmailTo())
                .withMessage(emailAWS.getMessage());
		
		SendEmailResult result = client.getStandardClient().sendEmail(request);
		LOGGER.info(String.valueOf(result.getSdkHttpMetadata().getHttpStatusCode()));
		return emails.save(emailRequest.toEmail(emailAWS));
	}
	
	public Email buscar(Long id) {
		return emails.findById(id);
	}
}
