package com.aws.email.dto;

import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;

public class EmailAWS {

	private Destination emailTo;
	
	private Content subject;
	
	private Content textBody;
	
	private Body body;
	
	private Message message;

	public Destination getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(Destination emailTo) {
		this.emailTo = emailTo;
	}

	public Content getSubject() {
		return subject;
	}

	public void setSubject(Content subject) {
		this.subject = subject;
	}

	public Content getTextBody() {
		return textBody;
	}

	public void setTextBody(Content textBody) {
		this.textBody = textBody;
	}

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}
}
