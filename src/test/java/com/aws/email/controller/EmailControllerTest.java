package com.aws.email.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.aws.email.TestBaseClass;
import com.aws.email.model.Email;
import com.aws.email.request.EmailRequest;
import com.aws.email.service.EmailService;

@RunWith(SpringRunner.class)
@WebMvcTest(EmailController.class)
public class EmailControllerTest extends TestBaseClass {
	
	@Autowired
    private MockMvc mvc;
 
    @MockBean
    private EmailService service;
    
    private String validJson;
    
    private Email testEmail;
    
    @Before
    public void before() {
    	validJson = parseJsonToRequest("json_valido.json", EmailRequest.class).toString();
    	testEmail = new Email(1L, "email1@email1.com", "mensagem","assunto");
    }
    
	@Test
    public void deveEnviarEmailComSucesso() throws Exception {
    	//similar ao when.thenReturn, mas o doReturn nao executa o metodo de fato
		doReturn(testEmail).when(service).enviar((EmailRequest)anyObject());
    	mvc.perform(post("/email").contentType(MediaType.APPLICATION_JSON).content(validJson))
    	.andExpect(status().isOk())
    	.andDo(print());
    }
    
    @Test
	public void deveEncontrarEmailComSucesso() throws Exception {
    	when(service.buscar(anyLong())).thenReturn(testEmail);
		
    	mvc.perform(get("/email/1"))
    	.andExpect(status().isOk())
    	.andExpect(jsonPath("$.id").exists())
		.andExpect(jsonPath("$.id", is(1)))
		.andExpect(jsonPath("$.emailTo", is("email1@email1.com")))
		.andExpect(jsonPath("$.message", is("mensagem")))
		.andExpect(jsonPath("$.subject", is("assunto")))
		.andDo(print());
    	
    	verify(service, times(1)).buscar(anyLong());
	}
}
