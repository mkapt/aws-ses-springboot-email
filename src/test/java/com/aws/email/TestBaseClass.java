package com.aws.email;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TestBaseClass {

	private final static String CAMINHO = "src/test/resources/json/";
	
	protected <T> T parseJsonToRequest(String file, Class<T> clazz) {
		try {
			Path pathObject = Paths.get(CAMINHO.concat(file));
			return new ObjectMapper().readValue(Files.readAllBytes(pathObject), clazz);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
