package com.aws.email.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_email")
public class Email {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private String emailTo;
	
	@Column(length=12000)
	private String message;
	
	private String subject;
	
	public Email(){};
	
	
	public Email(Long id, String emailTo, String message, String subject) {
		super();
		this.id = id;
		this.emailTo = emailTo;
		this.message = message;
		this.subject = subject;
	}
	
	public Email(String emailTo, String message, String subject) {
		super();
		this.emailTo = emailTo;
		this.message = message;
		this.subject = subject;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return id;
	}
	
	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
}
